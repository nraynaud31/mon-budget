import { createApp } from 'vue';
import * as VueRouter from 'vue-router';
import App from './App.vue'
import 'mdb-vue-ui-kit/css/mdb.min.css';
import HelloWorld from './components/HelloWorld.vue';
import Add from './components/Add.vue';
import Register from './components/Register.vue';
import Login from './components/Login.vue';


const routes = [
    { path: '/', component: HelloWorld },
    { path: '/add', component: Add },
    { path: '/register', component: Register },
    { path: '/login', component: Login },

];

const router = VueRouter.createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: VueRouter.createWebHashHistory(),
    routes, // short for `routes: routes`
})

createApp(App)
    .use(router)
    .mount('#app')
