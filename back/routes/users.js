const express = require('express');
const { SchemaTypes } = require('mongoose');
const router = express.Router();
const users = require('../models/User');
const bcrypt = require('bcrypt');
const JsonWebToken = require('jsonwebtoken');
const SECRET_JTWT_CODE = "psmR3HuOihHKfqZymolm";


/* GET users listing. */
router.get('/', (req, res) => {
  res.send('respond with a resource');
});

// Data de tests
router.post('/user', (req, res) => {
  let data = [
    {
      username: "John",
      password: "21",
      budget: 23.5,
    },
    {
      username: "Smith",
      password: "27",
      budget: 50.5,
    },
    {
      username: "Lisa",
      password: "23",
      budget: 1000.20,
    }
  ];
  // On insère la data en DB
  users.insertMany(data, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
})

// On récupère l'ensemble des users
router.get('/users', (req, res) => {
  users.find({}, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

// Infos du users en fonction de l'ID
router.get('/users/:id', (req, res) => {
  users.findById(req.params.id, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

// Mettre à jour le status en fonction de l'ID
router.post('/users/update/:id/?status=:stat', (req, res) => {
  users.findByIdAndUpdate(req.params.id, { "status": req.params.stat }, (err, result) => {
    if (err) {
      res.send(err)
    } else {
      res.send(result)
    }
  })
})

// Deletion du user en fonction de l'ID
router.post('/users/delete/:id', (req, res) => {
  users.findByIdAndDelete(req.params.id, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
})

// Auths

router.post('/user/signup', (req, res) => {
  if (!req.body.username || !req.body.password) {
    res.json({ success: false, error: "Need params" })
    return
  }
  let data = [{
    username: req.body.username,
    password: bcrypt.hashSync(req.body.password, 10),
  }]
  users.insertMany(data, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      const token = JsonWebToken.sign({ id: users._id, username: users.username }, SECRET_JTWT_CODE, {
        expiresIn: 120
      });
      res.json({ success: result, token: token });
    }
  })
})

router.post('/user/login', (req, res) => {
  if (!req.body.username || !req.body.password) {
    return res.json({ success: false, error: "Need params" })
  }
  users.findOne({ username: req.body.username })
    .then((user) => {
      if (!user) {
        res.json({ success: false, error: "user does not exist" })
      } else {
        if (!bcrypt.compareSync(req.body.password, user.password)) {
          res.json({ success: false, error: "Wrong password" })
        } else {
          const token = JsonWebToken.sign({ id: user._id, username: user.username }, SECRET_JTWT_CODE, {
            expiresIn: "120s"
          });
          res.json({ success: true, token: token })
        }
      }
    }).catch(err => {
      res.json({ success: false, error: err })
    })

})

// Permet de récupérer si le token est valide
function fetchUserByToken(req) {
  return new Promise((resolve, reject) => {
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
      let authorization = req.headers.authorization.split(" ")[1];
      let decoded
      try {
        decoded = JsonWebToken.verify(authorization, SECRET_JTWT_CODE)
      } catch (e) {
        return reject("Invalid token");
      }
      // On vérifie que l'user existe
      let userId = decoded.id
      users.findOne({ _id: userId })
        .then((user) => {
          resolve(user)
        })
        .catch((err) => {
          reject("Token error:", err)
        })
    } else {
      reject("Unknown Token");
    }
  })
}

// Vérifier l'existence du token
router.get('/verify', (req, res) => {
  fetchUserByToken(req)
    .then((user) => {
      res.send(user);
    }).catch((err) => {
      res.status(404).send("Token is invalid: ", err);
    })
})


module.exports = router;
