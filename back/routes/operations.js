const express = require('express');
const { SchemaTypes } = require('mongoose');
const router = express.Router();
const operations = require('../models/Operations');

/* GET category listing. */
router.get('/', (req, res) => {
  res.send('respond with a resource');
});

// Data de tests
router.post('/operation', (req, res) => {
  let data = [
    {
      title: "VIREMENT de test",
      montant: 40,
      month: "Janvier",
      category: "entrée",
      user: "62791bc7fcd131c576de9dbc",
    },
    {
      title: "Achat de ma voiture",
      montant: 40,
      month: "Janvier",
      category: "dépense",
      user: "62791bc7fcd131c576de9dbc",
    },
  ];
  // On insère la data en DB
  operations.insertMany(data, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
})

router.post('/addOperation', (req, res) => {
  let data = [
    {
      title: req.body.title,
      montant: req.body.montant,
      month: req.body.month,
      category: req.body.category,
      user: "62791bc7fcd131c576de9dbc",
    },
  ];
  // On insère la data en DB
  operations.insertMany(data, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
})

// On récupère l'ensemble des operations
router.get('/operations', (req, res) => {
  operations.find({}, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

// Infos du operations en fonction de l'ID
router.get('/operations/:id', (req, res) => {
  operations.findById(req.params.id, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

// Infos du operations en fonction de l'ID de l'user
router.get('/operations/user/:userId', (req, res) => {
  operations.find({ "user": { _id: req.params.userId } }, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

// Mettre à jour le name en fonction de l'ID
router.post('/operations/update/:id/?name=:name', (req, res) => {
  operations.findByIdAndUpdate(req.params.id, { "name": req.params.name }, (err, result) => {
    if (err) {
      res.send(err)
    } else {
      res.send(result)
    }
  })
})

// Deletion du user en fonction de l'ID
router.post('/operations/delete/:id', (req, res) => {
  operations.findByIdAndDelete(req.params.id, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
})


module.exports = router;
