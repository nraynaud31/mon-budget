const express = require('express');
const { SchemaTypes } = require('mongoose');
const router = express.Router();
const categories = require('../models/Categories');

/* GET category listing. */
router.get('/', (req, res) => {
  res.send('respond with a resource');
});

// Data de tests
router.post('/category', (req, res) => {
  let data = [
    {
      name: "dépense",
    },
    {
      name: "entrée",
    },
  ];
  // On insère la data en DB
  categories.insertMany(data, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
})

// On récupère l'ensemble des categories
router.get('/categories', (req, res) => {
  categories.find({}, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

// Infos du categories en fonction de l'ID
router.get('/categories/:id', (req, res) => {
  categories.findById(req.params.id, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

// Mettre à jour le name en fonction de l'ID
router.post('/categories/update/:id/?name=:name', (req, res) => {
  categories.findByIdAndUpdate(req.params.id, { "name": req.params.name }, (err, result) => {
    if (err) {
      res.send(err)
    } else {
      res.send(result)
    }
  })
})

// Deletion du user en fonction de l'ID
router.post('/categories/delete/:id', (req, res) => {
  categories.findByIdAndDelete(req.params.id, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
})


module.exports = router;
