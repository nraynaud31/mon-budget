const mongoose = require("mongoose");

const Schema = mongoose.Schema;


// On définit le schéma de notre DB
let category = new Schema(
    {
        name: {
            type: String
        },
    },
    { collection: "Categories" }
);

module.exports = mongoose.model("category", category);
