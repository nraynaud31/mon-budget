const mongoose = require("mongoose");

const Schema = mongoose.Schema;
let SchemaTypes = Schema.Types;


// On définit le schéma de notre DB
let user = new Schema(
    {
        username: {
            type: String
        },
        password: {
            type: String
        },
        budget: {
            type: Number
        },
    },
    { collection: "Users" }
);

module.exports = mongoose.model("user", user);
