const mongoose = require("mongoose");

const Schema = mongoose.Schema;
let SchemaTypes = Schema.Types;


// On définit le schéma de notre DB
let operations = new Schema(
    {
        title: {
            type: String
        },
        montant: {
            type: Number
        },
        month: {
            type: String
        },
        category: {
            type: String,
            ingredients: [
                { type: SchemaTypes.ObjectId, ref: 'category' }
            ],
        },
        user: {
            type: String,
            ingredients: [
                { type: SchemaTypes.ObjectId, ref: 'user' }
            ],
        },
    },
    { collection: "Operations" }
);

module.exports = mongoose.model("operations", operations);
